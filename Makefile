bootstrap:
	npm install

serve:
	firebase serve

deploy:
	firebase deploy

disable:
	firebase hosting:disable