import * as functions from 'firebase-functions';

const robotsAllow = `
sitemap: https://stan-fit.com/sitemap.xml
`

const robotsDisallow = `
User-agent : *
Disallow : /
`

export const robotsTxt = functions.https.onRequest((req, res) => {
    const host = req.header('X-Forwarded-Host');
    const txt = host === 'stan-fit.com' ? robotsAllow : robotsDisallow;
    res.type('txt')
        .set('Cache-Control', 'public, max-age=7200, s-maxage=3600')
        .send(txt.trim())
        .end();
});