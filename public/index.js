
Vue.component('js-accordion', {
  template:
    `
    <li>
      <a href='' class='faq_list' @click.prevent="isToggleOpened = !isToggleOpened">
        <div class='faq_list_q'>
          <div class="faq_list_wrapper">
            <span class="faq_list_cap">Q</span>
            <slot name="title"></slot>
          </div>
          <div class='faq_list_toggle'>
            <span></span>
            <span :class="{ open: isToggleOpened }"></span>
          </div>
        </div>
        <transition tag="div" @before-enter="beforeEnter" @enter="enter" @before-leave="beforeLeave"
          @leave="leave">
          <div v-if="isToggleOpened" class="faq_list_toggleWrapper">
            <div class='faq_list_a'>
              <span class="faq_list_cap">A</span>
              <slot name="text"></slot>
            </div>
          </div>
        </transition>
      </a>
    </li>
  `,

  data() {
    return {
      isToggleOpened: false
    }
  },

  methods: {
    beforeEnter: function (el) {
      el.style.height = '0';
    },
    enter: function (el) {
      el.style.height = el.scrollHeight + 'px';
    },
    beforeLeave: function (el) {
      el.style.height = el.scrollHeight + 'px';
    },
    leave: function (el) {
      el.style.height = '0';
    }
  }
})

const page = new Vue({
  el: '#page',

  data: {
    isCVBtnClicked: false,
    scrollY: '0',
    menuToggleFlag: false,

    // メールフォーム
    mail: "",
    company: "",
    name: "",
    matter: "commu",
    content: "",

    nameCheckFlag: true,
    contentCheckFlag: true,
    mailCheckBlankFlag: true,
    mailCheckRegFlag: true,

    step: 1
  },

  filters: {
    toMatterChar($v) {
      return $v === 'commu' ? 'コミュニティ参加希望' : 'その他のお問い合わせ'
    }
  },

  mounted() {
    window.addEventListener('scroll', this.handleScroll);
  },

  methods: {
    handleScroll() {
      this.scrollY = window.scrollY
    },

    sendMail() {
      this.step = 3
      const formData = new FormData();
      formData.append('メール', this.mail)
      formData.append('組織名', this.company ? this.company : '未記入')
      formData.append('氏名', this.name)
      formData.append('ご用件', this.matter === 'commu' ? 'コミュニティ参加希望' : 'その他のお問い合わせ')
      formData.append('内容', this.content)

      axios.post('mail.php', formData).then(res => {
        this.step = 3
      })
    }
  },

  watch: {
    scrollY($new) {
      $new > 6300 ? this.isCVBtnClicked = true : this.isCVBtnClicked = false
    },

    mail($v) {
      const reg = new RegExp(/^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
      $v ? this.mailCheckBlankFlag = true : this.mailCheckBlankFlag = false
      $v.match(reg) ? this.mailCheckRegFlag = true : this.mailCheckRegFlag = false
    },

    name($v) {
      $v ? this.nameCheckFlag = true : this.nameCheckFlag = false
    },

    content($v) {
      $v ? this.contentCheckFlag = true : this.contentCheckFlag = false
    }
  },

  computed: {
    allValidated() {
      return this.name && this.content && this.mail && this.mailCheckRegFlag
    }
  }
})









